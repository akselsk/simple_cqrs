defmodule DistributedCqrsTest do
  use ExUnit.Case
  alias DistributedCqrs.AggregateAgent
  alias DistributedCqrs.ReadModelAgent
  alias DistributedCqrs.FileDb
  alias DistributedCqrs.EventHandler

  setup_all do

    on_exit(fn ->
      File.rm_rf("read_model_db_test")
      File.rm_rf("stream_db_test")
    end)

    nodes = LocalCluster.start_nodes("my-cluster", 3)
    [node1, node2, node3] = nodes
     Node.ping(node3)
     Node.ping(node1)
     Node.ping(node2)
    :ok

  end


  ## Test aggregate
  test "Create and Dispatch" do
    {:ok, _} = Registry.register(DistributedCqrs.LocalPubSub, "new_event", [])

   assert CounterAggregate.dispatch("testid",%CounterAggregate.Create{}) == :ok
    assert CounterAggregate.dispatch("testid", %CounterAggregate.Increment{}) == :ok

   assert_become(fn -> CounterReadModel.get("testid") end, %CounterReadModel.State{
             count: 1
           }, 3000)
  end

  test "Create already created" do
    assert CounterAggregate.dispatch("testid2",%CounterAggregate.Create{}) == :ok
    assert CounterAggregate.dispatch("testid2",%CounterAggregate.Create{}) ==
             {:error, "already created"}
  end

  test "Increment not created" do
    assert CounterAggregate.dispatch("testid3",%CounterAggregate.Increment{ }) ==
             {:error, "Counter does not exist"}
  end

  test "Replay stream on boot" do
    assert CounterAggregate.dispatch("testid4",%CounterAggregate.Create{ }) == :ok
    AggregateAgent.kill(CounterAggregate, "testid4")
    :timer.sleep(100)

    assert CounterAggregate.dispatch("testid4",%CounterAggregate.Create{ }) ==
             {:error, "already created"}

  end

  ## Test RM
  test "Read rm state from db" do
  event = %CounterAggregate.Created{}
  metadata = %DistributedCqrs.EventMetadata{aggregate_id: "testid8", event_nr: 1}

  DistributedCqrs.EventHandler.dispatch({metadata,event})
   assert_become(fn -> CounterReadModel.get("testid8") end, %CounterReadModel.State{
             count: 0
           }, 3000)

    :ok = ReadModelAgent.kill(CounterReadModel, "testid8")
   assert_become(fn -> CounterReadModel.get("testid8") end, %CounterReadModel.State{
             count: 0
           }, 3000)
  end

  test "Writing to handled events" do
  event = %CounterAggregate.Created{}
  metadata = %DistributedCqrs.EventMetadata{aggregate_id: "testid7", event_nr: 1}
  DistributedCqrs.EventHandler.dispatch({metadata,event})

   assert_become(fn -> CounterReadModel.get("testid7") end, %CounterReadModel.State{
             count: 0
           }, 3000)


    tmp = DistributedCqrs.FileDb.get_handled_events(CounterReadModel)
    assert tmp["testid7"] == 1

   end

   defp assert_become(fun,value, timeout) do
       if timeout < 0 do
           assert fun.() == value
       else

       case fun.() do
           a when a == value -> assert fun.() == value
           a -> :timer.sleep(10)
           	assert_become(fun, value, timeout-10)
       end
   end
end


end
