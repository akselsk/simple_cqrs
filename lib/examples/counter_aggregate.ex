defmodule CounterAggregate do
  alias DistributedCqrs.AggregateAgent

  @moduledoc """
      Example of an aggregate
  """
  defmodule State, do: defstruct([:count])
  defmodule Create, do: defstruct([])
  defmodule Increment, do: defstruct([])

  defmodule Created, do: defstruct([])
  defmodule Incremented, do: defstruct([])


  ## Where should metadata lay? Inside event? Might be just as well
  ## Else it is implied

  alias CounterAggregate
  alias Aggregate

  def dispatch(id, %Create{} = cmd), do: AggregateAgent.dispatch(id, __MODULE__, cmd)
  def dispatch(id, %Increment{} = cmd), do: AggregateAgent.dispatch(id, __MODULE__, cmd)

  # Callbacks
  def execute(state, %Create{}) do
      IO.inspect state
    if state do
      {:error, "already created"}
    else
      {:ok, %Created{}}
    end
  end

  def execute(state, %Increment{}) do
    state
    |> case do
      nil ->
        {:error, "Counter does not exist"}

      _ ->
        {:ok, %Incremented{}}
    end
  end

  def apply_event(_, %Created{}), do: %State{count: 0}
  def apply_event(state, %Incremented{}), do: %State{count: state.count + 1}
end
