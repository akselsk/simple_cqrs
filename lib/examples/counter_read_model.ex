## Example of a read model
defmodule CounterReadModel do
  alias DistributedCqrs.ReadModelAgent
  defmodule State, do: defstruct([:count])


  def get(id), do: ReadModelAgent.get_state( __MODULE__,id)

  def should_replay, do: true

  def handle({metadata, %CounterAggregate.Created{} = event}),
    do: ReadModelAgent.handle(__MODULE__, metadata.aggregate_id, {metadata, event})

  def handle({metadata, %CounterAggregate.Incremented{} = event}),
    do: ReadModelAgent.handle(__MODULE__, metadata.aggregate_id, {metadata, event})

  def handle(_), do: :ok

  def apply_event(_, _, %CounterAggregate.Created{} = event),
    do: {:ok, %State{count: 0}}

  def apply_event(_, state, %CounterAggregate.Incremented{}),
    do: {:ok, %State{state | count: state.count + 1}}
end
