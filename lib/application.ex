defmodule DistributedCqrs.Application do
  use Application

    @event_handlers Application.get_env(:distributed_cqrs, :event_handlers, [])
    IO.inspect @event_handlers
  def start(_type, _args) do
    children =
      [
        {Horde.Registry, keys: :unique, name: :aggregate_registry, members: :auto},
        {Horde.Registry, keys: :unique, name: :read_model_registry, members: :auto},
	{Registry, keys: :duplicate, name: DistributedCqrs.LocalPubSub},

        # TODO Might need to make my own strategy for handling netsplit.. Ie, ask load balancer how many we are and require more than half
        {Horde.DynamicSupervisor,
         members: :auto,
         distributionstrategy: Horde.UniformQuorumDistribution,
         name: DistributedCqrs.DistributedAggregateSupervisor,
         strategy: :one_for_one},
        {Horde.DynamicSupervisor,
         members: :auto,
         distributionstrategy: Horde.UniformQuorumDistribution,
         name: DistributedCqrs.DistributedReadModelSupervisor,
         strategy: :one_for_one}
      ] ++ event_handlers_spec(@event_handlers)

    Supervisor.start_link(children, strategy: :one_for_one, name: DistributedCqrs.Supervisor)
  end

  defp event_handlers_spec(event_handlers),
    do: Enum.map(event_handlers, fn handler -> {DistributedCqrs.EventHandler, handler: handler} end)
end


  # def start_link(init_args) do
  #   [event_handlers: event_handlers] = init_args
  #   Supervisor.start_link(__MODULE__, event_handlers, name: __MODULE__)
  # end

  #@impl true
  #init
    # start_event_handlers(read_models, true)
    # start_event_handlers(side_effects, false)
