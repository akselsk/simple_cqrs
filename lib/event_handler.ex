defmodule DistributedCqrs.EventHandler do
  require Logger
  use GenServer
  @eventstore DistributedCqrs.FileDb
  @read_model_db DistributedCqrs.FileDb

  def start_link(args) do
    [handler: handler] = args

    GenServer.start_link(__MODULE__, handler,
      name: String.to_atom("EventHandler:" <> to_string(handler))
    )
  end

  def dispatch({metadata,event}) do
    Registry.dispatch(DistributedCqrs.LocalPubSub, "new_event", fn entries ->
      for {pid, _} <- entries, do: send(pid, {:new_event, {metadata,event}})
    end)
  end

  def replay_stream(stream_id), do: GenServer.cast(__MODULE__, {:replay_stream, stream_id})

  def init(handler) do
    {:ok, _} = Registry.register(DistributedCqrs.LocalPubSub, "new_event", [])

    if handler.should_replay do
      GenServer.cast(self(), :replay_streams)
    end

    {:ok, handler}
  end

  def handle_cast(:replay_streams, handler) do
    DistributedCqrs.ReadModelAgent.start_all(handler)
    handled_events = @read_model_db.get_handled_events(handler)
    @eventstore.get_all_stream_ids
    |> Enum.reduce(0, fn stream_id, acc -> replay_stream(stream_id, handler, handled_events) end )
    {:noreply, handler}
  end

  def handle_info({:new_event, {metadata,event}}, handler) do
    Logger.debug("Got event in event_handler #{inspect(to_string(handler))}")
    :ok = handler.handle({metadata,event})

    :ok =
      @read_model_db.update_handled_events(to_string(handler), metadata.aggregate_id, metadata.event_nr)
      
    {:noreply, handler}
  end


  def handle_cast({:replay_stream, id}, handler) do
    replay_stream(id, handler,%{})
    {:noreply, handler}
  end

  defp replay_stream(stream, handler, handled_events) do
   event_nr =
   case handled_events[stream] do
       nil -> 0
       x -> x+1
       end

    @eventstore.load(stream, event_nr)
    |> (fn {events, event_nr} -> events end).()
    |> Enum.map(fn event -> handler.handle(event) end)
  end


end
