defmodule DistributedCqrs.ReadModelAgent do
  use GenServer
  require Logger
  @read_model_db DistributedCqrs.FileDb

  def handle(model, id, event) do
    supervised_start(id, model)

    via_registry(model, id)
    |> GenServer.call({:update_state, event})
  end

  def get_state(model,id) do
    supervised_start(id, model)
    via_registry(model, id) |> GenServer.call(:get_state)
  end

  def stop(model, id) do
    via_registry(model, id) |> GenServer.cast(:end_process)
  end

  def kill(model, id) do
    via_registry(model, id) |> GenServer.cast(:kill)
  end

  def start_all(model) do
      @read_model_db.get_read_model_ids(model)
      |> Enum.map(fn id -> supervised_start(id,model) end)
  end


  def start_link([id: id, model: model] = _args) do
    case GenServer.start_link(__MODULE__, {model, id}, name: via_registry(model, id)) do
      {:ok, _} -> GenServer.cast(via_registry(model, id), :finish_init)
      {:error, {:already_started, _pid}} -> :ignore
      _ -> :error
    end
  end

  # callbacks
  def init({model, id}) do
    Logger.info(
      "Read model #{inspect(unique_id(model, id))} started on node: #{inspect(Node.self())}"
    )

    {:ok, %{model: model, id: id, handled_events: %{}, rm_state: %{}}}
  end

  def handle_cast(:finish_init, state) do
    @read_model_db.get(state[:model], state[:id])
    |> case do
      {derived_state, handled_events} ->
        {:noreply,
         Map.put(state, :rm_state, derived_state) |> Map.put(:handled_events, handled_events)}

      [] ->
        {:noreply, state}
    end
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state[:rm_state], state}
  end

  def handle_call({:update_state, {metadata, event}}, _from, state) do
    Logger.debug("Read model got a new event #{inspect(event)}")
    event_nr = metadata.event_nr
    handled_events = state[:handled_evnets]

    {response, new_state} =
      handled_events[:aggregate_id]
      |> case do
        a when a == event_nr -> {:ok, state}
        a when a == event_nr - 1 -> apply_event(state, metadata, event)
        nil -> apply_event(state, metadata, event)
        _ -> raise "Something is wrong!"
      end

    {:reply, response, new_state}
  end

  # Helpers

  defp apply_event(state, metadata, event) do
    model = state[:model]
    {:ok, new_rm_state} = model.apply_event(metadata, state[:rm_state], event)

    new_handled_events =
      Map.put(state[:handled_events], metadata.aggregate_id, metadata.event_nr)
    :ok = @read_model_db.commit(model, state[:id], {new_rm_state, new_handled_events})

    new_state =
      Map.put(state, :handled_events, new_handled_events)
      |> Map.put(:rm_state, new_rm_state)
    {:ok, new_state}
  end

  defp unique_id(model, id), do: to_string(model) <> "-" <> id

  def supervised_start(id, model) do
    spec = {DistributedCqrs.ReadModelAgent, id: id, model: model}
    Horde.DynamicSupervisor.start_child(DistributedCqrs.DistributedReadModelSupervisor, spec)
    DistributedCqrs.Helpers.ensure_started(unique_id(model, id), :read_model_registry)
  end

  defp via_registry(model, id) do
    {:via, Horde.Registry, {:read_model_registry, unique_id(model, id)}}
  end

  def handle_cast(:end_process, state) do
    {:stop, :normal, state}
  end

  def handle_cast(:kill, state) do
    {:stop, :normal, state}
  end


end

