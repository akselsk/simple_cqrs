defmodule DistributedCqrs.Helpers do
  def ensure_started(id, registry), do: ensure_started(id, registry, 4000)

  defp ensure_started(id, registry, timeout) do
    if timeout < 0 do
      raise "To late response from supervised registry"
    end

    Horde.Registry.lookup(registry, id)
    |> case do
      [] ->
        :timer.sleep(10)
        ensure_started(id, registry, timeout - 10)

      _ ->
        :ok
    end
  end
end
