defmodule DistributedCqrs.FileDb do
  require Logger

  @base_directory Application.get_env(:distributed, :stream_db_path, "stream_db_test/")
  @rm_base_directory Application.get_env(:distributed, :rm_db_path, "read_model_db_test/")

  def append(stream_id, event, event_counter) do
    if event_counter == 0 do
      File.mkdir_p(@base_directory <> "#{stream_id}")
    end

    File.write(
      @base_directory <> "#{stream_id}/#{event_counter |> to_string}",
      :erlang.term_to_binary(event)
    )
  end


  def load(stream_id), do: load(stream_id, 0)
  def load(stream_id, nil), do: load(stream_id, 0)

  def load(stream_id, event_counter) do
    files = list_files(@base_directory <> "#{stream_id}")

    files
    |> Enum.split(event_counter)
    |> (fn {[], filtered_files} ->
          read_files(@base_directory <> "#{stream_id}/", filtered_files)
        end).()
    |> (fn events -> {events |> Enum.reverse, Kernel.length(files) } end).()
  end

  defp read_files(base, files), do: Enum.map(files, fn file -> read_file(base <> file) end)
  defp read_file(file), do: File.read!(file) |> :erlang.binary_to_term()

  def get_all_stream_ids do
    IO.puts("getting all ids from filedb")
    list_files(@base_directory)
  end

  def get(model, uuid) do
    File.read(@rm_base_directory <> to_string(model) <> "/" <> uuid)
    |> case do
      {:ok, binary} ->
        {derived_state, event_ids} = :erlang.binary_to_term(binary)
        {derived_state, event_ids}

      _ ->
        []
    end
  end

  def commit(model, uuid, state) do
      File.mkdir_p!(@rm_base_directory <> to_string(model))

      File.write(
      @rm_base_directory <> to_string(model) <> "/" <> uuid,
      :erlang.term_to_binary(state)
    )
  end

  def get_read_model_ids(model), do: list_files(@rm_base_directory <> to_string(model))


  def update_handled_events(handler, stream_id, event_nr) do
    path = @rm_base_directory <> "#{handler}/handled_events/#{stream_id}"
    expected_nr = event_nr - 1

    File.read(path)
    |> case do
      {:error, :enoent} -> File.write(path, :erlang.term_to_binary(event_nr))
      {:ok, expected_nr} -> File.write(path, :erlang.term_to_binary(event_nr))
    end

    :ok
  end

  def get_handled_events(handler) do
    read_event_nr_from_file = fn file ->
      {:ok, binary} = File.read(@rm_base_directory <> "#{handler}/handled_events/#{file}")
      :erlang.binary_to_term(binary)
    end

    list_files(@rm_base_directory <> "#{handler}/handled_events")
    |> Enum.reduce(%{}, fn file, acc -> Map.put(acc, file, read_event_nr_from_file.(file)) end)
  end


  defp list_files(path) do
    File.ls(path)
    |> case do
      {:ok, files} ->
        files

      {:error, :enoent} ->
        File.mkdir_p!(path)
        []
    end
  end
end
