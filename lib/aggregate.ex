defmodule DistributedCqrs.AggregateAgent do
  require Logger
  use GenServer, restart: :transient
  @eventstore DistributedCqrs.FileDb

  def dispatch(id,model,command) do
    supervised_start(id, model)
    via_registry(model, id) |> GenServer.call(command)
  end

  def supervised_start(aggregate_id, model) do
    spec = {DistributedCqrs.AggregateAgent, aggregate_id: aggregate_id, model: model, name: via_registry(model,aggregate_id)}
    Horde.DynamicSupervisor.start_child(DistributedCqrs.DistributedAggregateSupervisor, spec)
    DistributedCqrs.Helpers.ensure_started(unique_id(model,aggregate_id), :aggregate_registry)
  end

  def start_link(args) do
    [aggregate_id: aggregate_id, model: model, name: name] = args
    #name = via_registry(model, aggregate_id)

    case GenServer.start_link(__MODULE__, {model, aggregate_id}, name: name) do
      {:ok, _} ->
          	GenServer.cast(name, :finish_init)
      {:error, {:already_started, pid}} -> Logger.info("already started at #{inspect(pid)}, returning :ignore")
        :ignore
    end
  end


  def stop(model, aggregate_id) do
    via_registry(model, aggregate_id) |> GenServer.cast(:end_process)
  end

  def kill(model, aggregate_id) do
    via_registry(model, aggregate_id) |> GenServer.cast(:kill)
  end

  defp via_registry(model, aggregate_id) do
    {:via, Horde.Registry, {:aggregate_registry, unique_id(model, aggregate_id)}}
  end

  # callbacks
  def init({model, aggregate_id}) do
      Logger.info("Aggregate #{inspect(unique_id(model,aggregate_id))} started on node #{inspect(Node.self())}")
    {:ok, %{model: model, event_nr: 0, aggregate_id: aggregate_id, aggregate_state: %{}}}
  end

  def handle_cast(:end_process, state) do
    {:stop, :normal, state}
  end

  def handle_cast(:kill, state) do
    {:stop, :kill, state}
  end

  def handle_cast(:finish_init, state) do
    {events, event_nr} = @eventstore.load(to_string(state[:model]) <> "-" <> state[:aggregate_id])
    Map.put(state, :event_nr, event_nr)
    |> Map.put(:aggregate_state, state_from_events(state[:model],events))
    |> noreply
  end

  def handle_call(command, _from, state) do
      model = state[:model]
    case model.execute(state[:aggregate_state],command) do
      {:error, reason} ->
        {:reply, {:error, reason}, state}

      {:ok, event} ->
          new_state =
          Map.put(state, :aggregate_state, model.apply_event(state[:aggregate_state], event))
          |> Map.put(:event_nr, state[:event_nr]+1)

          @eventstore.append( to_string(model) <> "-" <> state[:aggregate_id], event,
          new_state[:event_nr] )
        DistributedCqrs.EventHandler.dispatch({%DistributedCqrs.EventMetadata{aggregate_id: new_state[:aggregate_id], event_nr: new_state[:event_nr]}, event})
        {:reply, :ok, new_state}
    end
  end

  defp unique_id(model,id), do: to_string(model) <> "-" <> id
  defp state_from_events(model, events) do
    events
    |> Enum.reduce(nil, fn event, state ->
      model.apply_event(state, event)
    end)
  end

  defp noreply(state), do: {:noreply, state}

end
