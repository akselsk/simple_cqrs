defmodule DistributedCqrs.EventMetadata do
  defstruct [:aggregate_id, :event_nr]
end
