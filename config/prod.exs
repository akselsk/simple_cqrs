use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.

# Print only warnings and errors during test
config :logger, level: :warn

config :distributed_cqrs, read_model_handlers: [CounterReadModel]
