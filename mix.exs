defmodule DistributedCqrs.MixProject do
  use Mix.Project

  def project do
    [
      app: :distributed_cqrs,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: [
        test: "test --no-start"

      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {DistributedCqrs.Application, []},
      extra_applications: [:logger],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1"},
      {:horde, git: "https://github.com/derekkraan/horde"},
      {:elixir_uuid, "~> 1.2"},
      {:local_cluster, "~> 1.1", only: [:test]}

    ]
  end
end
